﻿
#include <time.h>
#include <stdio.h>
#include <math.h>
#include <windows.h>
double factorial(int f)
{
	if (f == 0)
		return(1);
	return(f * factorial(f - 1));
}


int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	printf("f(n) = n\t\t f(n) = log(n)\t\t f(n) = n*log(n)\t\t f(n) = n^2\t\tf(n) = 2^n\t\t f(n) = n!\n\n");
	double y, y1, y2, y3, y4, y5;
	for (int n = 0; n <= 50;n++)
	{
		y = n;
		printf("f(%d) = %g\t\t", n, y);
		y1 = log(n);
		printf("f(%d) = %.2f\t\t", n, y1);
		y2 = n * log(n);
		printf("f(%d) = %4.2f\t\t", n, y2);
		y3 = pow(n, 2);
		printf("f(%d) = %g\t\t", n, y3);
		y4 = pow(2, n);
		printf("f(%d) = %2.f\t\t", n, y4);
		y5 = factorial(n);
		printf("f(%d) = %2.f\n", n, y5);
	}

}

