﻿#include <iostream>
#include <chrono>
#include <time.h>
#include <stdlib.h>

#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>
unsigned long long factorial(unsigned long long a) {
    if (a == 0 || a == 1)
        return 1;
    else
        return a * factorial(a - 1);
}
void bubbleSort(float arr[], int n) {
    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            if (arr[j] < arr[j + 1]) {
              
                float temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}

int main() {
  
   

    for (unsigned long long i = 0;i <= 20;i++) {
        auto begin = GETTIME();
        unsigned long long res = factorial(i);
        auto end = GETTIME();
        auto elapsed_ns = CALCTIME(end - begin);
        printf("Value: %-3d | Result: %-12llu | Runtime: %-lld ns\n", i, res, elapsed_ns.count());

    }
      
 

    srand(time(NULL));
 int arr1[] = { 50,100,200,300,400,500,600,700,800,900,1000 };
    for (int i = 0; i < 11; i++) {
        
        printf("Random floating-point numbers generated in the range from -100 to 100 (Size: %d):\n", arr1[i]);
        float* arr = (float*)malloc(arr1[i] * sizeof(float));

        for (int j = 0; j < arr1[i]; j++) {
            arr[j] = -100.0f + ((float)rand() / RAND_MAX) * 200.0f;
        }

        auto begin1 = GETTIME();
        bubbleSort(arr, arr1[i]);
        auto end1 = GETTIME();

       

        auto elapsed_ns1 = CALCTIME(end1 - begin1);
        printf(" Runtime [%d]: %lld ns\n\n", arr1[i], elapsed_ns1.count());

        free(arr);
    }

    return 0;
}

